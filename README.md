Winner of the 2011 La Recherche popular science prize and French Industry Award 2013, Antescofo~ for [Max](https://cycling74.com/products/max/) and [PureData](https://puredata.info) is a modular polyphonic [Score Following](http://repmus.ircam.fr/score-following) system as well as a [Synchronous Programming](http://repmus.ircam.fr/synchrone) language for real-time computer music composition and live performance. The module allows for automatic recognition of music score position and tempo from a realtime audio Stream coming from performer(s), making it possible to synchronize an instrumental performance with computer realized elements. The synchronous language within Antescofo allows flexible writing of time and interaction in computer music in conjuction with its dedicated graphical editor [AscoGraph](https://forum.ircam.fr/projects/detail/ascograph-the-antescofo-graphical-editor).


Since its public launch, Antescofo has been integrated in various new pieces involving human musicians and computers at IRCAM and beyond and on international scenes.

## Fields of Application ##

- pedagogy: interactive accompaniment applications,

- composition: interactive electronic score writing linking a musical performance and computer generated sonic material,

- scientific research and development: performer playing and tempo analysis,

- computer music: “smart sequencer”, musical programming,

- amateur: automatic accompaniment.

## Main Features ##

Antescofo accepts its own score format of a piece of music. It is possible to automatically convert existing formats such as MIDI or MusicXML to Antescofo format using AscoGraph.

Once the score is loaded, Antescofo is capable of following the position and tempo of live performer(s) and undertaking binded electronic actions synchronously. Antescofo’s real time input is raw polyphonic audio by default but can be adapted to user defined inputs such as MIDI, raw pitch (in Hz), or other inputs.

The synchronous language of Antescofo allows coupling live performances (in position and timing) to that of electronic commands programmed by artists and composers.

> - **Note** 
>
>The PureData package has limited functionalities, help documents and unsupported.[Contact authors](https://www.antescofo.com/fr/contactez-nous/) if you wish to contribute.
>
> - Available for Max and PureData
>
> - Developped by [MuTant Team-Project](http://repmus.ircam.fr/mutant)
>
> - [Video](https://medias.ircam.fr/stream/ext/video/old_archives/video/VI02024400-237.webm)